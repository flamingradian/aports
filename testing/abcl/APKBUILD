# Maintainer: Celeste <cielesti@protonmail.com>
pkgname=abcl
pkgver=1.9.2
pkgrel=0
pkgdesc="Armed Bear Common Lisp"
url="https://www.abcl.org/"
# armhf, armv7: takes more than 6 hours to build
# riscv64, loongarch64: blocked by apache-ant & jdk
arch="noarch !armhf !armv7 !riscv64 !loongarch64"
license="GPL-2.0-or-later WITH Classpath-exception-2.0"
depends="java-jna"
makedepends="apache-ant java-jdk"
source="https://www.abcl.org/releases/$pkgver/abcl-src-$pkgver.tar.gz
	fix-tests.patch
	system-java-jna.patch
	"
builddir="$srcdir/abcl-src-$pkgver"

case "$CARCH" in
	# Newer Java is not supported on 32bit.
	x86|arm*) depends="$depends openjdk8-jre-base" ;;
	*) depends="$depends java-jre-headless" ;;
esac

build() {
	ant abcl.compile
	ant abcl.jar
}

check() {
	ant abcl.test
}

package() {
	install -Dm755 /dev/stdin "$pkgdir"/usr/bin/abcl <<-EOF
	#!/bin/sh
	exec java -jar /usr/share/java/abcl.jar "\$@"
	EOF

	install -Dvm644 dist/*.jar -t "$pkgdir"/usr/share/java/
}

sha512sums="
1b758b26be5dbe00fb75aef0ffca6cdfbbb7982a545c63b6378dbd3d22bed76e15628ee099c5728c3600d67fb74def249195060cc1af1486f6d52fcdb9ce245a  abcl-src-1.9.2.tar.gz
81d561199c66bea0dc59a6165a94c17b731047208e7a828c15d16f33eb8a7d8d0720601d85cfcd034dc829ee2496085700a44aab2d6a8c7e33a2e9263f60a1ab  fix-tests.patch
0e9d8a2548f5f0d7a2850d4ea1c77964ed460eed49b26f104967b1682e595c894e193d56c500ec3935418e5a9ebe738adb4e5f4c626132eea83cb8ce14115a34  system-java-jna.patch
"
