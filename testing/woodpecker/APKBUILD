# Contributor: Carlo Landmeter <clandmeter@alpinelinux.org>
# Contributor: Patrycja Rosa <alpine@ptrcnull.me>
# Maintainer: Will Sinatra <wpsinatra@gmail.com>
pkgname=woodpecker
pkgver=2.6.0
pkgrel=0
pkgdesc="Simple yet powerful CI/CD engine with great extensibility"
url="https://woodpecker-ci.org"
arch="all"
license="Apache-2.0"
pkgusers="woodpecker"
pkggroups="woodpecker"
makedepends="
	go
	go-swag
	sqlite-dev
	"
subpackages="$pkgname-doc $pkgname-openrc"
install="$pkgname.pre-install"
source="$pkgname-$pkgver.tar.gz::https://github.com/woodpecker-ci/woodpecker/releases/download/v$pkgver/woodpecker-src.tar.gz
	0001-we-use-the-prebuild-webui.patch
	woodpecker.initd
	woodpecker.confd
	woodpecker.conf
	fix-cgo.patch
	swagger-cgo.patch
	"
options="net"

export GOFLAGS="$GOFLAGS -tags=libsqlite3"
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

unpack() {
	mkdir -p $builddir
	tar -xzvf $srcdir/$pkgname-$pkgver.tar.gz -C $builddir
}

build() {
	export TARGETARCH="$(go env GOHOSTARCH)"
	make build -j1 STATIC_BUILD=false
}

check() {
	# temporarily skip failed test in TestStringSliceAddToMap
	# at cmd/agent/core/agent_test.go:56-60,69
	go test -skip="TestStringSliceAddToMap/empty string in slice" ./...
}

package() {
	install -Dm755 "$builddir"/dist/woodpecker-server \
		"$pkgdir"/usr/bin/woodpecker-server
	install -Dm755 "$builddir"/dist/woodpecker-agent \
		"$pkgdir"/usr/bin/woodpecker-agent
	install -Dm755 "$builddir"/dist/woodpecker-cli \
		"$pkgdir"/usr/bin/woodpecker-cli
	install -Dm 644 "$builddir"/LICENSE \
		"$pkgdir"/usr/share/licenses/$pkgname/LICENSE

	install -Dm755 "$srcdir"/woodpecker.initd \
		"$pkgdir"/etc/init.d/woodpecker
	install -Dm644 "$srcdir"/woodpecker.confd \
		"$pkgdir"/etc/conf.d/woodpecker

	install -Dm640 -o woodpecker -g woodpecker "$srcdir"/woodpecker.conf \
		"$pkgdir"/etc/woodpecker.conf
	install -dm755 -o woodpecker -g woodpecker "$pkgdir"/var/lib/woodpecker
}

sha512sums="
70b6fbafd19221e98f63bbfb5feae1ab6219c04d5288146f66fb3e3e9f0c86b88456a052ca4ac8c7af7dd331854881925c42fdbf0078be42e97465546d6e2e15  woodpecker-2.6.0.tar.gz
aecf28a0e893ef39f4a9c834aab6d9cdfef5a7f01ba5918134e2e7f3784ae102e642b9e247957da9c480ac7486b5f82027bff8df3033e90a503de57597b69917  0001-we-use-the-prebuild-webui.patch
69fe477f805dcb71b0220b9af2b3d0226b2e92f3c46764f37a139bb7303ad7cdb1caa2a711d1f9d22fccb357bbfbecb1c6cba2033c9101a11c0bb67b405c3e55  woodpecker.initd
0be91432e730cb0ad3663bebe7a257437cbefe5fa5c2f3145d621545d6cd2ff89ae41f338a5874166d2b03dc8caab73d26cd4322ed1122d4949cae5d6002b823  woodpecker.confd
cb15d7ff290d9b68d5f63c20401ab622c8a7067d336841c876a6d3325e9d2a3ede3a85b792131d7d77a4126cbdb6f30a5a6113468f1efd736a2c1bbf2bfefbe4  woodpecker.conf
b0d03a8199de796579e4a15aaf586048720e95b8253e91668ed4d03cf1e0819eb47638dca2ef54f67b461ec8349feb9452aad7a4e43accce7d4f9eea4b50ffa1  fix-cgo.patch
a646ba7f6f96625df77c5b5fb4bf8bb61a40b8d92d49787f73daf0f35900a1ea71555cb13f7e82ee0c5725b319ff112ee717c5ec729a0c285217f2218ce938f3  swagger-cgo.patch
"
