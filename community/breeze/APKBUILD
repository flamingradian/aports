# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=breeze
pkgver=6.1.2
pkgrel=0
pkgdesc="Artwork, styles and assets for the Breeze visual style for the Plasma Desktop"
# armhf blocked by qt6-qtdeclarative
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
depends_dev="
	frameworkintegration-dev
	frameworkintegration5-dev
	kcmutils-dev
	kcolorscheme-dev
	kconfig-dev
	kconfig5-dev
	kconfigwidgets5-dev
	kcoreaddons-dev
	kcoreaddons5-dev
	kdecoration-dev
	kguiaddons-dev
	kguiaddons5-dev
	ki18n-dev
	kiconthemes-dev
	kiconthemes5-dev
	kirigami-dev
	kirigami2-dev
	kwindowsystem-dev
	kwindowsystem5-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtx11extras-dev
	qt6-qtbase-dev
	qt6-qtdeclarative-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
subpackages="$pkgname-dev $pkgname-lang"
_repo_url="https://invent.kde.org/plasma/breeze.git"
source="https://download.kde.org/stable/plasma/$pkgver/breeze-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
cedcdeb06a001faeb585460ca489090c877a081c9999324d7f7847bdcc7197f22ca60a94de5a9950990429b8eadb66859c99f706462503673313fb3d50e550fc  breeze-6.1.2.tar.xz
"
