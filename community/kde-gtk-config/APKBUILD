# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=kde-gtk-config
pkgver=6.1.2
pkgrel=0
pkgdesc="GTK2 and GTK3 Configurator for KDE"
# armhf blocked by qt6-qtdeclarative
arch="all !armhf"
url="https://invent.kde.org/plasma/kde-gtk-config"
license="GPL-2.0 AND LGPL-2.1-only OR LGPL-3.0-only"
depends="gsettings-desktop-schemas"
makedepends="
	extra-cmake-modules
	gsettings-desktop-schemas-dev
	gtk+2.0-dev
	gtk+3.0-dev
	karchive-dev
	kcmutils-dev
	kconfigwidgets-dev
	kdecoration-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	knewstuff-dev
	qt6-qtbase-dev
	qt6-qtsvg-dev
	samurai
	sassc
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
_repo_url="https://invent.kde.org/plasma/kde-gtk-config.git"
source="https://download.kde.org/stable/plasma/$pkgver/kde-gtk-config-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
3daaa1d0409a64b79c2a6155cafea115d1526abfb8d2690877e9d79a27de2913c836fbf6adfca4e8c1fd027c36ce9da92529558585b54473dfca0a6c4185746a  kde-gtk-config-6.1.2.tar.xz
"
