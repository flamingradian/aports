# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=kmenuedit
pkgver=6.1.2
pkgrel=0
pkgdesc="KDE menu editor"
# armhf blocked by qt6-qtdeclarative
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kdbusaddons-dev
	kdoctools-dev
	kglobalaccel-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kitemviews-dev
	kxmlgui-dev
	qt6-qtbase-dev
	samurai
	sonnet-dev
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
subpackages="$pkgname-doc $pkgname-lang"
_repo_url="https://invent.kde.org/plasma/kmenuedit.git"
source="https://download.kde.org/stable/plasma/$pkgver/kmenuedit-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
2c6bfa08c45b7f80776ed3d515ebf5fb0f54ca981d44ad7c6d9ee760bd0bebefee77f2cffa2f55ec306106e293956b8e8386b933d56de7a3b841f2263eab6a60  kmenuedit-6.1.2.tar.xz
"
