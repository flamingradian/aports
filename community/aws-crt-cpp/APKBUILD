# Contributor: Holger Jaekel <holger.jaekel@gmx.de>
# Maintainer: fossdd <fossdd@pwned.life>
pkgname=aws-crt-cpp
pkgver=0.27.0
pkgrel=0
pkgdesc="C++ wrapper around the aws-c-* libraries. Provides Cross-Platform Transport Protocols and SSL/TLS implementations for C++"
url="https://github.com/awslabs/aws-crt-cpp"
# s390x: aws-c-common
# arm*, ppc64le: aws-c-io
arch="all !armhf !armv7 !ppc64le !s390x"
license="Apache-2.0"
depends_dev="
	$pkgname=$pkgver-r$pkgrel
	aws-c-auth-dev
	aws-c-cal-dev
	aws-c-common-dev
	aws-c-compression-dev
	aws-c-event-stream-dev
	aws-c-http-dev
	aws-c-io-dev
	aws-c-mqtt-dev
	aws-c-s3-dev
	aws-c-sdkutils-dev
	aws-checksums-dev
	s2n-tls-dev
	"
makedepends="
	$depends_dev
	cmake
	samurai
	"
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/awslabs/aws-crt-cpp/archive/refs/tags/v$pkgver.tar.gz"
options="net"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		local crossopts="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	CFLAGS="$CFLAGS -flto=auto" \
	CXXFLAGS="$CXXFLAGS -flto=auto" \
	cmake -B build -G Ninja\
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=/usr/lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=None \
		-DBUILD_DEPS=False \
		-DBUILD_TESTING="$(want_check && echo ON || echo OFF)" \
		$crossopts
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure -j${JOBS:-2}
}

package() {
	DESTDIR="$pkgdir" cmake --install build
	# just test binaries
	# shellcheck disable=2115
	rm -rf "$pkgdir"/usr/bin/
}

dev() {
	default_dev
	amove usr/lib/aws-crt-cpp
}

sha512sums="
07d04eb5d2c4434f60063671d4ed7cfd8801b841f3139a961737749750e5af663bf01e95cdec7c623b706e0b6a24dee5e6a102ff5887a08a9420de26f2d65fce  aws-crt-cpp-0.27.0.tar.gz
"
